const express = require('express')
const uid = require('uid')
const app = express()

app.use(express.json()) //wenn ein request ankommt, der ein json string enthält, mach da ein javascript objekt draus

const data = {
  cards: [
    {
      title: '{Names} Skills:',
      content: 'lorem ipsum',
      id: uid(),
      tags: ['example 1', 'example 2', 'example 3', 'example 4'],
    },
    {
      title: '{Names} Skills:',
      content: 'lorem ipsum',
      id: uid(),
      tags: ['example 1', 'example 2', 'example 3', 'example 4'],
    },
    {
      title: '{Names} Skills:',
      content: 'lorem ipsum',
      id: uid(),
      tags: ['example 1', 'example 2', 'example 3', 'example 4'],
    },
  ],
}

app.get('/cards', (req, res) => {
  res.json(data.cards)
})

app.post('/cards', (req, res) => {
  //das Array soll mit einer random id angereichert werden, wenn sie vom server kommt
  const newCard = req.body
  newCard.id = uid()
  data.cards.push(newCard)
  res.json(newCard)
})

app.delete('/cards/:id', (req, res) => {
  const id = req.param.id
  const deletedCard = data.cards.find(card => card.id === id)
  data.cards = data.cards.filter(card => card.id !== id)
  res.json(deletedCard)
})

// schickt einen Datensatz, der einen bestehenden Datensatz unter der id ersetzen soll
app.put('/cards/:id', (req, res) => {
  const id = req.params.id
  const replaceCardIndex = data.cards.findIndex(card => card.id === id)
  const card = { ...req.body, id: uid() }
  res.json(card)
})

//updated einen Datensatz
app.patch('/cards/:id', (req, res) => {
  const id = req.params.id
  const index = data.cards.findIndex(card => card.id === id)
  const card = { ...data.cards[index], ...req.body }
  data.cards[index] = card
  res.json(card)
})

app.listen(process.env.PORT || 3000, () => {
  console.log('Server ready on port 3000')
})
